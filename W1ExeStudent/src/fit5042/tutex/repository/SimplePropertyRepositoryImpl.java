/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository;

import fit5042.tutex.repository.entities.Property;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO Exercise 1.3 Step 2 Complete this class.
 * 
 * This class implements the PropertyRepository class. You will need to add the keyword
 * "implements" PropertyRepository. 
 * 
 * @author Junyang
 */
public class SimplePropertyRepositoryImpl implements PropertyRepository {
	
	ArrayList<Property> properties;

    public SimplePropertyRepositoryImpl(ArrayList<Property> properties) {
		super();
		this.properties = properties;
	}
    
	public SimplePropertyRepositoryImpl() {
		this.properties = new ArrayList<Property>();
	}

	public ArrayList<Property> getProperties() {
		return properties;
	}

	public void setProperties(ArrayList<Property> properties) {
		this.properties = properties;
	}

	@Override
	public void addProperty(Property property) throws Exception {
		// TODO Auto-generated method stub
		properties.add(property);
	}

	@Override
	public Property searchPropertyById(int id) throws Exception {
		// TODO Auto-generated method stub
		Property searchProperty = properties.get(id - 1);
		return searchProperty;
	}

	@Override
	public List<Property> getAllProperties() throws Exception {
		// TODO Auto-generated method stub
		ArrayList<Property> getAllProperties = new ArrayList<>();
		for(int i = 0 ; i < properties.size(); i++)
		{
			getAllProperties.add(properties.get(i));
		}
		return getAllProperties;
	}
	
	

	
    
}
